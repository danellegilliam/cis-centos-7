control 'cis-centos-7-1.1.1.1' do
  impact 1.1
  title 'Ensure mounting of cramfs filesystems is disabled'
  desc 'The cramfs filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A cramfs image can be used without having to first decompress the image'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install cramfs \/bin\/true/ }
  end
  describe kernel_module('cramfs') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.2' do
  impact 1.1
  title 'Ensure mounting of freevxfs filesystems is disabled'
  desc 'The freevxfs filesystem type is a free version of the Veritas type filesystem. This is the primary filesystem type for HP-UX operating systems.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install freevxfs \/bin\/true/ }
  end
  describe kernel_module('freevxfs') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.3' do
  impact 1.1
  title 'Ensure mounting of jffs2 filesystems is disabled'
  desc 'The jffs2 (journaling flash filesystem 2) filesystem type is a log-structured filesystem used in flash memory devices.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install jffs2 \/bin\/true/ }
  end
  describe kernel_module('jffs2') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.4' do
  impact 1.1
  title 'Ensure mounting of hfs filesystems is disabled'
  desc 'The hfs filesystem type is a hierarchical filesystem that allows you to mount Mac OS filesystems.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install hfs \/bin\/true/ }
  end
  describe kernel_module('hfs') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.5' do
  impact 1.1
  title 'Ensure mounting of hfsplus filesystems is disabled'
  desc 'The hfsplus filesystem type is a hierarchical filesystem designed to replace hfs that allows you to mount Mac OS filesystems.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install hfsplus \/bin\/true/ }
  end
  describe kernel_module('hfsplus') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.6' do
  impact 1.1
  title 'Ensure mounting of squashfs filesystems is disabled'
  desc 'The squashfs filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems (similar to cramfs ). A squashfs image can be used without having to first decompress the image.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install squashfs \/bin\/true/ }
  end
  describe kernel_module('squashfs') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.7' do
  impact 1.1
  title 'Ensure mounting of udf filesystems is disabled'
  desc 'The udf filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install udf \/bin\/true/ }
  end
  describe kernel_module('udf') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.1.8' do
  impact 2.2
  title 'Ensure mounting of FAT filesystems is disabled'
  desc 'The FAT filesystem format is primarily used on older windows systems and portable USB drives or flash modules. It comes in three types FAT12 , FAT16 , and FAT32 all of which are supported by the vfat kernel module.'
  describe file('/etc/modprobe.d/CIS.conf') do
    its('content') { should match /install vfat \/bin\/true/ }
  end
  describe kernel_module('vfat') do
    it { should_not be_loaded }
  end
end

control 'cis-centos-7-1.1.2-1.1.5' do
  impact 2.2
  title 'Ensure separate partition exists for /tmp and specific options are set'
  desc 'The /tmp directory is a world-writable directory used for temporary storage by all users and some applications.'
  describe mount('/tmp') do
    it { should be_mounted }
    its('options') { should eq ['rw', 'nosuid', 'nodev', 'noexec', 'relatime'] }
  end
end

control 'cis-centos-7-1.1.6' do
  impact 2.2
  title 'Ensure separate partition exists for /var'
  desc 'The /var directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.'
  describe mount('/var') do
    it { should be_mounted }
  end
end

control 'cis-centos-7-1.1.7-1.1.10' do
  impact 2.2
  title 'Ensure separate partition exists for /var/tmp and specific options are set'
  desc 'The /var/tmp directory is a world-writable directory used for temporary storage by all users and some applications.'
  describe mount('/var/tmp') do
    it { should be_mounted }
    its('options') { should eq ['nodev', 'nosuid', 'noexec'] }
  end
end

control 'cis-centos-7-1.1.11' do
  impact 2.2
  title 'Ensure separate partition exists for /var/log'
  desc 'The /var/log directory is used by system services to store log data .'
  describe mount('/var/log') do
    it { should be_mounted }
  end
end

control 'cis-centos-7-1.1.12' do
  impact 2.2
  title 'Ensure separate partition exists for /var/log/audit'
  desc 'The auditing daemon, auditd , stores log data in the /var/log/audit directory.'
  describe mount('/var/log/audit') do
    it { should be_mounted }
  end
end

control 'cis-centos-7-1.1.13-1.1.14' do
  impact 2.2
  title 'Ensure separate partition exists for /home and specific options are set'
  desc 'The /home directory is used to support disk storage needs of local users.'
  describe mount('/home') do
    it { should be_mounted }
    its('options') { should eq ['nodev'] }
  end
end

control 'cis-centos-7-1.1.15-1.1.17' do
  impact 1.1
  title 'Ensure nodev, nosuid, and noexec options set on /dev/shm partition'
  desc 'The nodev mount option specifies that the filesystem cannot contain special devices. The nosuid mount option specifies that the filesystem cannot contain setuid files. The noexec mount option specifies that the filesystem cannot contain executable binaries.'
  describe mount('/dev/shm') do
    its('options') { should eq ['nodev', 'nosuid', 'noexec'] }
  end
end

control 'cis-centos-7-1.1.21' do
  impact 1.1
  title 'Ensure sticky bit is set on all world-writable directories'
  desc 'Setting the sticky bit on world writable directories prevents users from deleting or renaming files in that directory that are not owned by them.'
  describe command('df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d ( -perm -0002 -a ! -perm -1000 ) 2>/dev/null') do
    its('stdout') { should eq '' }
  end
end

control 'cis-centos-7-1.1.22' do
  impact 1.2
  title 'Disable Automounting'
  desc 'autofs allows automatic mounting of devices, typically including CD/DVDs and USB drives.'
  describe systemd_service('autofs') do
    it { should_not be_enabled }
  end
end

control 'cis-centos-7-1.2.3' do
  impact 1.1
  title 'Ensure gpgcheck is globally activated'
  desc 'The gpgcheck option, found in the main section of the /etc/yum.conf and individual /etc/yum/repos.d/* files determines if an RPM package\'s signature is checked prior to its installation.'
  describe command('grep ^gpgcheck /etc/yum.conf') do
    its('stdout') { should eq ('/gpgcheck=1/') }
  end
  describe command('grep ^gpgcheck /etc/yum.repos.d/*') do
    its('stdout') { should eq ('/gpgcheck=1/') }
  end
end

control 'cis-centos-7-1.3.1' do
  impact 1.1
  title 'Ensure AIDE is installed'
  desc 'AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.'
  describe package('aide') do
    it { should be_installed }
  end
end

control 'cis-centos-7-1.3.2' do
  impact 1.1
  title 'Ensure filesystem integrity is regularly checked'
  desc 'Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.'
  describe crontab('root') do
    its('hours') { should cmp '5' }
    its('minutes') { should cmp '0' }
    its('days') { should cmp '*' }
    its('weekdays') { should cmp '*' }
    its('months') { should cmp '*' }
    its('commands') { should include '/usr/sbin/aide --check' }
  end
end

control 'cis-centos-7-1.4.1' do
  impact 1.1
  title 'Ensure permissions on bootloader config are configured'
  desc 'The grub configuration file contains information on boot settings and passwords for unlocking boot options. The grub configuration is usually located at /boot/grub2/grub.cfg and linked as /etc/grub2.cfg. Additional settings can be found in the /boot/grub2/user.cfg file.'
  describe file('/boot/grub2/grub.cfg') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    it ('mode') { should cmp '0600' }
  end
  describe file('/boot/grub2/user.cfg') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    it ('mode') { should cmp '0600' }
  end
end

control 'cis-centos-7-1.4.2' do
  impact 1.1
  title 'Ensure bootloader password is set'
  desc 'Setting the boot loader password will require that anyone rebooting the system must enter a password before being able to set command line boot parameters'
  describe file('/boot/grub2/grub.cfg') do
    its('content') { should match %r{/GRUB2_PASSWORD=.*/} }
  end
end

control 'cis-centos-7-1.4.3' do
  impact 1.1
  title 'Ensure authentication required for single user mode'
  desc 'Single user mode (rescue mode) is used for recovery when the system detects an issue during boot or by manual selection from the bootloader.'
  describe file('/boot/grub2/grub.cfg') do
    its('content') { should match %r{/GRUB2_PASSWORD=.*/} }
  end
end

control 'cis-centos-7-1.5.1' do
  impact 1.1
  title 'Ensure core dumps are restricted'
  desc 'A core dump is the memory of an executable program. It is generally used to determine why a program aborted. It can also be used to glean confidential information from a core file. The system provides the ability to set a soft limit for core dumps, but this can be overridden by the user.'
  describe file('/etc/security/limits.conf') do
    its('content') { should match /* hard core 0/ }
  end
  describe file('/etc/security/limits.d/*') do
    its('content') { should match /* hard core 0/ }
  end
  describe kernel_parameter('fs.suid_dumpable') do
    its('value') { should eq 0 }
  end
  describe file('/etc/sysctl.conf') do
    its('content') { should match /fs.suid_dumpable = 0/ }
  end
  describe file('/etc/sysctl.d/*') do
    its('content') { should match /fs.suid_dumpable = 0/ }
  end
end

control 'cis-centos-7-1.5.2' do
  impact 1.1
  title 'Ensure XD/NX support is enabled'
  desc 'Recent processors in the x86 family support the ability to prevent code execution on a per memory page basis. Generically and on AMD processors, this ability is called No Execute (NX), while on Intel processors it is called Execute Disable (XD). This ability can help prevent exploitation of buffer overflow vulnerabilities and should be activated whenever possible. Extra steps must be taken to ensure that this protection is enabled, particularly on 32-bit x86 systems. Other processors, such as Itanium and POWER, have included such support since inception and the standard kernel for those platforms supports the feature.'
  describe command('dmesg | grep NX') do
    its('stdout') { should match (/NX (Execute Disable) protection: active/) }
  end
end

control 'cis-centos-7-1.5.3' do
  impact 1.1
  title 'Ensure address space layout randomization (ASLR) is enabled'
  desc 'Address space layout randomization (ASLR) is an exploit mitigation technique which randomly arranges the address space of key data areas of a process.'
  describe kernel_parameter('kernel.randomize_va_space') do
    its('value') { should eq 2 }
  end
  describe file('/etc/sysctl.conf') do
    its('content') { should match /kernel.randomize_va_space = 2/ }
  end
  describe file('/etc/sysctl.d/*') do
    its('content') { should match /kernel.randomize_va_space = 2/ }
  end
end

control 'cis-centos-7-1.5.4' do
  impact 1.1
  title 'Ensure prelink is disabled'
  desc 'prelinkis a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.'
  describe package('prelink') do
    it { should_not be_installed }
  end
end

control 'cis-centos-7-1.6.1.1' do
  impact 2.2
  title 'Ensure SELinux is not disabled in bootloader configuration'
  desc 'Configure SELINUX to be enabled at boot time and verify that it has not been overwritten by the grub boot parameters.'
  describe file('/boot/grub2/grub.cfg') do
    its('content') { should_not match /selinux=0/ }
    its('content') { should_not match /enforcing=0/ }
  end
end

control 'cis-centos-7-1.6.1.2' do
  impact 2.2
  title 'Ensure the SELinux state is enforcing'
  desc 'Set SELinux to enable when the system is booted.'
  describe file('/etc/selinux/config') do
    its('content') { should match /SELINUX=enforcing/ }
  end
  describe command('sestatus') do
    its('stdout') { should match (/Current mode: enforcing/) }
  end
end

control 'cis-centos-7-1.6.1.3' do
  impact 2.2
  title 'Ensure SELinux policy is configured'
  desc 'Configure SELinux to meet or exceed the default targeted policy, which constrains daemons and system software only.'
  describe file('/etc/selinux/config') do
    its('content') { should match /SELINUXTYPE=targeted|SELINUXTYPE=mls/ }
  end
  describe command('sestatus') do
    its('stdout') { should match (/SELINUXTYPE=targeted|SELINUXTYPE=mls/) }
  end
end

control 'cis-centos-7-1.6.1.4' do
  impact 2.0
  title 'Ensure SETroubleshoot is not installed'
  desc 'The SETroubleshoot service notifies desktop users of SELinux denials through a user-friendly interface. The service provides important information around configuration errors, unauthorized intrusions, and other potential errors.'
  describe package('setroubleshoot') do
    it { should_not be_installed }
  end
end

control 'cis-centos-7-1.6.1.5' do
  impact 2.2
  title 'Ensure the MCS Translation Service (mcstrans) is not installed'
  desc 'The mcstransd daemon provides category label information to client processes requesting information. The label translations are defined in /etc/selinux/targeted/setrans.conf'
  describe package('mcstrans') do
    it { should_not be_installed }
  end
end

control 'cis-centos-7-1.6.1.6' do
  impact 2.2
  title 'Ensure no unconfined daemons exist'
  desc 'Daemons that are not defined in SELinux policy will inherit the security context of their parent process.'
  describe command('ps -eZ | egrep "initrc" | egrep -vw "tr|ps|egrep|bash|awk" | tr \':\' \' \' | awk \'{ print $NF }\'') do
    its('stdout') { should eq '' }
  end
end

control 'cis-centos-7-1.6.2' do
  impact 2.2
  title 'Ensure SELinux is installed'
  desc 'SELinux provides Mandatory Access Controls.'
  describe package('libselinux') do
    it { should be_installed }
  end
end

control 'cis-centos-7-1.7.1.1' do
  impact 2.2
  title 'Ensure message of the day is configured properly'
  desc 'The contents of the /etc/motd file are displayed to users after login and function as a message of the day for authenticated users.
Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If mingetty(8) supports the following options, they display operating system information: \m - machine architecture \r - operating system release \s - operating system name \v - operating system version'
  describe file('/etc/motd') do
    its('content') { should_not match /(\\v|\\r|\\m|\\s)/ }
  end
end

control 'cis-centos-7-1.7.1.2-3' do
  impact 1.1
  title 'Ensure local and remote login warning banner is configured properly'
  desc 'The contents of the /etc/issue file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If mingetty(8) supports the following options, they display operating system information: \m - machine architecture \r - operating system release \s - operating system name \v - operating system version'
  describe file('/etc/issue') do
    its('content') { should_not match /(\\v|\\r|\\m|\\s)/ }
  end
end

control 'cis-centos-7-1.7.1.4' do
  impact 1.1
  title 'Ensure permissions on /etc/motd are configured'
  desc 'The contents of the /etc/motd file are displayed to users after login and function as a message of the day for authenticated users.'
  describe file('/etc/motd') do
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0644' }
  end
end

control 'cis-centos-7-1.7.1.5' do
  impact 1.1
  title 'Ensure permissions on /etc/issue are configured'
  desc 'The contents of the /etc/issue file are displayed to users prior to login for local terminals.'
  describe file('/etc/issue') do
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0644' }
  end
end

control 'cis-centos-7-1.7.1.6' do
  impact 1.1
  title 'Ensure permissions on /etc/issue.net are configured'
  desc 'The contents of the /etc/issue.net file are displayed to users prior to login for remote connections from configured services.'
  describe file('/etc/issue.net') do
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0644' }
  end
end

control 'cis-centos-7-1.7.2' do
  impact 1.1
  title 'Ensure GDM login banner is configured'
  desc 'GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.'
  describe file('/etc/dconf/profile/gdm') do
    its('content') { should match /user-db:user/ }
    its('content') { should match /system-db:gdm/ }
    its('content') { should match /file-db:/usr/share/gdm/greeter-dconf-defaults/ }
  end
  describe command('grep banner-message-enable=true /etc/dconf/db/gdm.d/*') do
    its('stdout') { should match /banner-message-enable=true/ }
  end
  describe command('grep banner-message-text= /etc/dconf/db/gdm.d/*') do
    its('stdout') { should match %r{/banner-message-text=.*/} }
  end
end

control 'cis-centos-7-1.8' do
  impact 1.1
  title 'Ensure updates, patches, and additional security software are installed'
  desc 'Periodically patches are released for included software either due to security flaws or to include additional functionality.'
  describe command('yum check-update --security') do
    its('stdout') { should eq '' }
  end
end
